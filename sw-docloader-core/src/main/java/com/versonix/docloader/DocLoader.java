package com.versonix.docloader;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

class DocLoader {

    private final Connection conn;
    private final String schema;
    private SWVersion version;


    public DocLoader(Connection conn, String schema) throws SQLException, IllegalArgumentException {
        this.conn = conn;
        this.schema = schema;

        if (this.schema == null) {
            throw new IllegalArgumentException("Schema cannot be null");
        }
        if (this.conn == null) {
            throw new IllegalArgumentException("Connection cannot be null");
        }
        try (Statement st = conn.createStatement()) {
            ResultSet result = st.executeQuery("SELECT " + this.schema + ".SW_VERSION() FROM DUAL");
            if (result.next()) {
                String strVersion = result.getString(1);
                if (strVersion != null) {
                    version = new SWVersion(strVersion);
                } else {
                    throw new IllegalArgumentException("Version is not present in Database");
                }
            }
        }
    }
    boolean dropSection(String section) throws SQLException {
        // Sections are supported
        if (version.compareTo("04_11_13.*.*") > 0) {
            try(PreparedStatement st = conn.prepareStatement("DELETE from " + schema + ".DOC_STORAGE WHERE \"SECTION\"=?")) {
                st.setString(1, section);
                st.execute();
            }
        }
        return  true;
    }

    boolean dropDoc(Document doc) throws SQLException {
        return dropDoc(doc.getDocFileName());
    }

    boolean dropDoc(String fileName) throws SQLException {
        // Sections are supported
        try (PreparedStatement st = conn.prepareStatement("DELETE from " + schema + ".DOC_STORAGE WHERE \"DOC_FILE_NAME\"=?")) {
            st.setString(1, fileName);
            st.execute();
            return  true;
        }
    }

    private String fieldValue(String[] arr, int idx, String def) {
        if (arr.length>idx) {
            String val = arr[idx].trim();
            return !"".equals(val)? val : def;
        } else {
            return def;
        }
    }

    /**
     * Processes files in versonix Fle format :
     * < Filename >   | < Section > |< UI Show Y/N > | < Doc Name >
     * @param fleFile     path to .FLE file
     * @param section     Section name
     * @param dropSection Drop all existing files in section first
     * @return number of files processed
     */
    int processFle(Path fleFile, String section, boolean dropSection) throws IOException, SQLException {
        int count = 0;
        Path basepath = fleFile.getParent();
        List<String> lines = Files.readAllLines(fleFile);
        List<Document> toLoad = new ArrayList<>(lines.size());
        for (int i = 0; i < lines.size(); i++) {
            String line = lines.get(i);
            String[] fields = line.split("\\|");
            if (fields.length > 0) {
                Path fileToLoad = basepath.resolve(fields[0].trim());
                if (Files.exists(fileToLoad)) {
                    Document.Builder b = Document.getBuilder().
                            setSection(section).
                            setDocArea(fieldValue(fields, 1, null)).
                            setIsUIShow("Y".equals(fieldValue(fields, 2, "N").toUpperCase())).
                            setDocName(fieldValue(fields, 3, null)).
                            setSection(fieldValue(fields, 3, section)).
                            setDocFileName(fieldValue(fields, 5, null)).
                            setFile(fileToLoad);

                    toLoad.add(b.build());
                } else {
                    throw new IllegalArgumentException(
                            String.format("file %s is not found at line %d", fileToLoad, i)
                    );
                }
            } else {
                throw new IllegalArgumentException(
                        String.format("unreadable data on row %d", i)
                );
            }
        }
        if (section != null && dropSection) {
            dropSection(section);
        }
        for (Document document : toLoad) {
            loadDoc(document);
        }
        return lines.size();
    }

    boolean loadDoc(Document doc) throws SQLException, IOException {
        // drop document first
        conn.setAutoCommit(false);
        dropDoc(doc);
        Blob blob = conn.createBlob();
        try ( OutputStream out = blob.setBinaryStream(1);
              InputStream in = new FileInputStream(doc.getFile());
              PreparedStatement st = conn.prepareStatement(
                      "INSERT INTO " + schema + ".DOC_STORAGE "+
                      "(\"DOC_NAME\", \"DOC_DATA\", \"DOC_FILE_NAME\", \"DOC_FORMAT\", \"IS_OLE\", \"DOC_AREA\", \"SECTION\", \"UI_SHOW\", \"DOC_FILE_NAME_TYPED\") "+
                      "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)")
        ) {
            st.setString(1, doc.getDocName());
            byte[] bytes = new byte[2048];
            int num, len = 0;
            while ((num = in.read(bytes)) > 0) {
                out.write(bytes, len, num);
                len += num;
            }
            out.flush();
            out.close();
            st.setBlob(2, blob);
            st.setString(3, doc.getDocFileName());
            st.setString(4, doc.getFormat());
            st.setString(5, doc.isOLE() ? "Y" : "N");
            st.setString(6, doc.getDocArea());
            st.setString(7, doc.getSection());
            st.setString(8, doc.isUIShow() ? "Y" : "N");
            st.setString(9, doc.getTypedFileName());
            st.execute();
            conn.commit();
        } catch (Exception e) {
            conn.rollback();
        } finally {
            conn.setAutoCommit(false);
        }

        return true;
    }


}

