package com.versonix.docloader;

class SWVersion implements Comparable<SWVersion> {
    private String major;
    private String minor;
    private String build;

    public SWVersion(String versionString) {
        major = "*";
        minor = "*";
        build = "*";
        if (versionString != null) {
            String[] versions = versionString.split("\\.");
            if (versions.length>0) {
                major = versions[0];
            }
            if (versions.length>1) {
                minor = versions[1];
            }
            if (versions.length>2) {
                build = versions[2];
            }
        }
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getMinor() {
        return minor;
    }

    public void setMinor(String minor) {
        this.minor = minor;
    }

    public String getBuild() {
        return build;
    }

    public void setBuild(String build) {
        this.build = build;
    }

    /**
     * Slightly extended version of compareTo
     * Returns 0 if versions match,
     * Returns -1 or +1 if difference in getBuilder numbers
     * Returns -2 or +2 if difference in minor version
     * Returns -3 or +3 if difference in major version
     * @param value Version to compare to
     * @return 0 if equals param, 1 if greater than param, -1 if less than param
     */
    public int compareTo(SWVersion value) {
        // we like *, it always matches
        if ("*".equals(value.major) || "*".equals(major)) {
            return 0;
        }
        int res = major.compareTo(value.major);
        if(res == 0) {
            if ("*".equals(minor) || "*".equals(value.minor)) {
                return 0;
            }
            res = minor.compareTo(value.minor);
            if (res == 0) {
                if ("*".equals(build) || "*".equals(value.build)) {
                    return 0;
                } else {
                    return build.compareTo(value.build)>0 ? 1 : -1;
                }
            } else {
                return res > 0 ? 2 : -2;
            }
        } else {
            return res > 0 ? 3 : -3;
        }
    }

    /**
     * @param version
     * @return
     */
    public int compareTo(String version) {
        return compareTo(new SWVersion(version));
    }
}
