package com.versonix.docloader;

import java.io.File;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Document {

    private File file;
    private String docName;
    private String docFileName;
    private String section;
    private String format;
    private String docArea;
    private boolean isOLE = false;
    private boolean isUIShow = false;

    public Document() { }

    public static class Builder {
        Document doc;

        private Builder() {
            doc = new Document();
        }
        public Builder setFile(String file) {
            return this.setFile(new File(file));
        }
        public Builder setFile(File file) {
            doc.file = file;
            if (file!=null) {
                if (doc.docName==null) {
                    setDocName(file.getName());
                }
                if (doc.docFileName==null) {
                    setDocFileName(file.getName());
                }
                if (doc.format==null) {
                    setFormat(file.getName().substring(file.getName().lastIndexOf('.')+1).toUpperCase());
                }
            }
            return this;
        }
        public Builder setFile(URI file) {
            return this.setFile(Paths.get(file));
        }
        public Builder setFile(Path file) {
            return this.setFile(file.toFile());
        }
        public Builder setSection(String section) {
            doc.section = section;
            return this;
        }
        public Builder setFormat(String format) {
            doc.format = format;
            return this;
        }
        public Builder setDocArea(String docArea) {
            doc.docArea = docArea;
            return this;
        }
        public Builder setDocName(String docName) {
            doc.docName = docName;
            return this;
        }
        public Builder setDocFileName(String docFileName) {
            doc.docFileName = docFileName;
            return this;
        }
        public Builder setIsOLE(boolean isOLE) {
            doc.isOLE = isOLE;
            return this;
        }
        public Builder setIsUIShow(boolean isUIShow) {
            doc.isUIShow = isUIShow;
            return this;
        }
        public Document build(){
            return doc;
        }
    }
    public static Builder getBuilder() {
       return new Builder();
    }

    public File getFile() {
        return file;
    }

    public String getFileName() {
        return getTypedFileName().toUpperCase();
    }

    public String getTypedFileName() {
        return file.getName();
    }

    public String getDocFileName() {
        return docFileName;
    }

    public String getDocName() {
        return docName;
    }

    public String getSection() {
        return section;
    }

    public String getFormat() {
        return format;
    }

    public String getDocArea() {
        return docArea;
    }

    public boolean isOLE() {
        return isOLE;
    }

    public boolean isUIShow() {
        return isUIShow;
    }

}
