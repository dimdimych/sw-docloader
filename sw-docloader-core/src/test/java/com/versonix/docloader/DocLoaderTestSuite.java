package com.versonix.docloader;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        DocLoaderTest.class
})
public class DocLoaderTestSuite {
}
