package com.versonix.docloader;

import org.junit.Test;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static org.junit.Assert.*;

/**
 * Created by dimak on 10/14/15.
 */
public class DocLoaderTest {
    private DocLoader loader;
    public DocLoader getLoader() throws SQLException {
        if (loader == null) {
            System.setProperty("oracle.net.tns_admin", "/media/shares/p/Ora");
            String dsn = System.getProperty("com.versonix.util.dsn", "jdbc:oracle:thin:@ORADEV");
            String schema = System.getProperty("com.versonix.util.schema", "SW_DIMAK_TEST1");
            String user = System.getProperty("com.versonix.util.user", schema);
            String password = System.getProperty("com.versonix.util.password", "init");
            if (schema != null) {
                Connection conn = DriverManager.getConnection(dsn, user, password);
                loader = new DocLoader(conn, schema);
            } else {
                throw new IllegalArgumentException("Schema is missing");
            }
        }
        return loader;
    }
    @Test
    public void testDocument() throws URISyntaxException {
        URL url = this.getClass().getResource("/xml_test.xml");
        Document doc = Document.getBuilder().setFile(url.toString()).build();
        assertEquals("XML_TEST.XML", doc.getFileName());
        assertEquals("xml_test.xml", doc.getTypedFileName());
        assertEquals("xml_test.xml", doc.getDocName());
        assertEquals("XML", doc.getFormat());
        assertNull(doc.getDocArea());
    }
    @Test
    public void testVersion() {
        SWVersion v1 = new SWVersion("15_08_09.99.01");
        SWVersion v2 = new SWVersion("04_08_08.*.*");
        SWVersion v3 = new SWVersion("15_08_09.99.02");
        assertTrue(v1.compareTo("*.*.*") == 0);
        assertTrue(v1.compareTo(v2) == 3);
        assertTrue(v1.compareTo(v3) == -1);


    }
    @Test
    public void testDropSection() throws Exception {
        getLoader().dropSection("TEST");
    }

    @Test
    public void testLoadDoc() throws Exception {
        Document doc = Document.getBuilder().setFile(
                Paths.get(
                    this.getClass().getResource("/xml_test.xml").toURI()
                )
        ).build();
        getLoader().loadDoc(doc);

    }



}