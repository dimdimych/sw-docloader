package com.versonix.docloader;

import java.io.IOException;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dimak on 1/25/16.
 */
public class Main {
    public Main() {

    }

    public int process(String[] args) throws SQLException, IOException {
        int res = 0;
        if (getBoolArg("h", "help", args)) {
            usage();
        } else {
            String dsn = getStrArg("d", "dsn", args);
            String schema = getStrArg("s", "schema", args);
            String user = getStrArg("u", "user", args);
            String password = getStrArg("p", "password", args);
            String section = getStrArg("t", "section", args, "DEFAULT");
            List<String> rest = new ArrayList<>(args.length);
            for (String arg : args) {
                if (arg != null) {
                    if (arg.startsWith("-")) {
                        throw new IllegalArgumentException("Argument " + arg + " is unexpected");
                    } else {
                        rest.add(arg);
                    }
                }
            }
            if (rest.size() == 0) {
                throw new IllegalArgumentException("DOCFILE is not specified");
            }
            boolean drop = getBoolArg("x", "delete", args);
            if (schema != null) {
                Connection conn = DriverManager.getConnection(dsn, user, password);
                DocLoader loader = new DocLoader(conn, schema);
                for (String s : rest) {
                    res += loader.processFle(Paths.get(s), section, drop);
                }
            } else {
                throw new IllegalArgumentException("Schema is missing");
            }
        }
        return res;
    }

    public static void main(String[] args) throws SQLException {
        if (System.getenv("TNS_ADMIN") != null) {
            System.setProperty("oracle.net.tns_admin", System.getenv("TNS_ADMIN"));
        }
        Main me = new Main();
        try {
            System.out.println(String.format("Processed %d files", me.process(args)));
        } catch (Exception e) {
            System.out.println("Error : "+e.getMessage());
            usage();
        }
    }
    private static void usage() {
        System.out.println("USAGE : java -jar sw-docloader.jar [OPTIONS] [DOCLIST.FLE] ... ");
        System.out.println("  -d DSN,     --dsn=DSN          Java JDBC connection string");
        System.out.println("  -s SCHEMA,  --schema=SCHEMA    Schema name");
        System.out.println("  -u USER,    --user=USER        Database Username");
        System.out.println("  -p PWD,     --password=PWD     Database Password");
        System.out.println("  -t SECTION, --section=SECTION  Doclibrary Section Name");
        System.out.println("  -x ,        --delete           Cleanup section (ignored if section option is missing)");
        System.out.println("DOCLIST - files in FLE format. Fomat of each line in FLE file is the following:");
        System.out.println("  FILENAME RELATIVE TO FLE | DOCAREA | UI_SHOW | DOC_NAME | SECTION | DOC_FILE_NAME ");
    }

    private static String getStrArg(String sh, String ln, String[] args) {
        return getStrArg(sh, ln, args, null);
    }
    private static String getStrArg(String sh, String ln, String[] args, String def) {
        for (int i = 0; i < args.length; i++) {
            String arg = args[i];
            if (sh != null && ("-" + sh).equals(arg) && args.length>i+1) {
                args[i] = null;
                args[i+1] = null;
                return args[i+1];
            } else if (ln != null && arg!=null && arg.startsWith("--"+ln+"=")) {
                args[i] = null;
                return arg.substring(ln.length()+3);
            }
        }
        return def;
    }
    private static boolean getBoolArg(String sh, String ln, String[] args) {
        return getBoolArg(sh, ln, args, false);
    }

    private static boolean getBoolArg(String sh, String ln, String[] args, boolean def) {
        for (int i = 0; i < args.length; i++) {
            String arg = args[i];
            if (arg!=null) {
                if (sh != null && ("-" + sh).equals(arg)) {
                    args[i] = null;
                    return true;
                } else if (ln != null && arg.equals("--" + ln)) {
                    args[i] = null;
                    return true;
                }
            }
        }
        return def;
    }
}
